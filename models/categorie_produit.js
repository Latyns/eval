'use strict';
module.exports = (sequelize, DataTypes) => {
  const Categorie_produit = sequelize.define('Categorie_produit', {
    nom: DataTypes.STRING
  }, {});
  Categorie_produit.associate = function(models) {
    // associations can be defined here
  };
  return Categorie_produit;
};