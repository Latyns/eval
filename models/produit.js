'use strict';
module.exports = (sequelize, DataTypes) => {
  const Produit = sequelize.define('Restaurant', {
    nom: DataTypes.STRING,
    prix: DataTypes.STRING,
    description: DataTypes.TEXT,
    image: DataTypes.STRING
  }, {});
  Restaurant.associate = function(models) {
    // associations can be defined here
  };
  return Restaurant;
};